#React Redux Secure

a package which decides on displaying components based on your rules.
I used react-redux connect function codebase

###Installation
```s
npm i -S react-redux-secure
```

a glimpse on how you handle your stuff would be easy as this. 
for better clarifications see example folder and run it by.

clone this project then 
```js
npm install && npm run example
```

###rules.js
```js
export default {
	comments: () => ({user}, ok, failed) => {
		if (user.rules.comments === true)
			return ok("comments");

		return failed("comments");
	},
	editComments: () => ({user}, ok, failed) => {
		if (user.rules.editComments === true)
			return ok("editComments");

		return failed("editComments");
	}
}
```

###store.js
```js
import {combineReducers, createStore, applyMiddleware} from "redux";
import {counterReducer} from "./reducers/counterReducer";
import userReducer from "./reducers/userReducer";
import createLogger from "redux-logger";
import {secureReducer} from "react-redux-secure";

export const store = createStore(
	combineReducers({
		counter: counterReducer,
		user: userReducer,
		secure: secureReducer(require("../rules").default)
	}),
	applyMiddleware(createLogger())
);
```

###Comments.jsx
```js
import React, {Component} from "react";
import {connect} from "react-redux";
import {secure} from "react-redux-secure";

@connect(({counter}) => ({counter}))
@secure(({user}, {comments, editComments}, run) => run(editComments(), comments()))
export default class Comments extends Component {
	render() {
		return (
			<div>comments</div>
		);
	}
}
```

also its good to have permission controll over small ui part in a bigger component.

see securify
```js
import {secure} from "react-redux-secure";
import React, {Component} from "react";

export function securify(InSecuredComponent, secureCallback) {
    @secure(secureCallback)
    class SecuredComponent extends Component {
        render() {
            return <InSecuredComponent/>
        }
    }

    return <SecuredComponent/>;
}
```
how to user securify then ?
```
{securify(
    () => <Link to="/v1/publisher/role">
	<i className='fa fa-user'/>
	<span className='title'> رول ها</span>
	<span className="selected"/>
    </Link>,
    ({user}, {canSeeSidebarRoleList}, run) => run(canSeeSidebarRoleList())
)}
```

###MIT licence
  
