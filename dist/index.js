"use strict";

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _secure = require("./secure");

var _secure2 = _interopRequireDefault(_secure);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function secureReducer(rules) {
	_secure2.default.prototype.rules = rules;
	return function () {
		var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
		var action = arguments[1];

		switch (action.type) {
			case "USER_IMPERSONATE":
				return state;
		}

		return state;
	};
}

module.exports = {
	secure: _secure2.default,
	secureReducer: secureReducer
};