"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = secure;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _storeShape = require("react-redux/lib/utils/storeShape");

var _storeShape2 = _interopRequireDefault(_storeShape);

var _shallowEqual = require("react-redux/lib/utils/shallowEqual");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _hoistNonReactStatics = require("hoist-non-react-statics");

var _hoistNonReactStatics2 = _interopRequireDefault(_hoistNonReactStatics);

var _invariant = require("invariant");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultMapStateToProps = function defaultMapStateToProps(state) {
	return {};
}; // eslint-disable-line no-unused-vars
var defaultMapDispatchToProps = function defaultMapDispatchToProps(dispatch) {
	return { dispatch: dispatch };
};
var defaultMergeProps = function defaultMergeProps(stateProps, dispatchProps, parentProps) {
	return _extends({}, parentProps, stateProps, dispatchProps);
};

function getDisplayName(WrappedComponent) {
	return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

var errorObject = { value: null };
function tryCatch(fn, ctx) {
	try {
		return fn.apply(ctx);
	} catch (e) {
		errorObject.value = e;
		return errorObject;
	}
}

function runCreator(state) {
	var counter = 0;
	var failed = [];
	return function run() {
		for (var _len = arguments.length, rules = Array(_len), _key = 0; _key < _len; _key++) {
			rules[_key] = arguments[_key];
		}

		if (rules.length == 0) throw new Error("provide some rules for run function");

		rules.forEach(function (rule) {
			rule(state, function (ruleName) {
				return counter += 1;
			}, function (ruleName) {
				return failed.push(ruleName);
			});
		});

		return !failed.length > 0;
	};
}

// Helps track hot reloading.
var nextVersion = 0;
function secure(mapStateToProps, mapDispatchToProps, mergeProps) {
	var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

	var shouldSubscribe = Boolean(mapStateToProps);
	var mapState = mapStateToProps || defaultMapStateToProps;

	var mapDispatch = void 0;
	if (typeof mapDispatchToProps === 'function') {
		mapDispatch = mapDispatchToProps;
	} else if (!mapDispatchToProps) {
		mapDispatch = defaultMapDispatchToProps;
	} else {
		mapDispatch = wrapActionCreators(mapDispatchToProps);
	}

	var finalMergeProps = mergeProps || defaultMergeProps;
	var _options$pure = options.pure,
	    pure = _options$pure === undefined ? true : _options$pure,
	    _options$withRef = options.withRef,
	    withRef = _options$withRef === undefined ? false : _options$withRef;

	var checkMergedEquals = pure && finalMergeProps !== defaultMergeProps;

	// Helps track hot reloading.
	var version = nextVersion++;

	return function wrapWithSecure(WrappedComponent) {
		var secureDisplayName = "Secure(" + getDisplayName(WrappedComponent) + ")";

		function computeMergedProps(stateProps, dispatchProps, parentProps) {
			var mergedProps = finalMergeProps(stateProps, dispatchProps, parentProps);

			return mergedProps;
		}

		var Secure = function (_Component) {
			_inherits(Secure, _Component);

			_createClass(Secure, [{
				key: "shouldComponentUpdate",
				value: function shouldComponentUpdate() {
					return !pure || this.haveOwnPropsChanged || this.hasStoreStateChanged;
				}
			}]);

			function Secure(props, context) {
				_classCallCheck(this, Secure);

				var _this = _possibleConstructorReturn(this, (Secure.__proto__ || Object.getPrototypeOf(Secure)).call(this, props, context));

				_this.version = version;
				_this.store = props.store || context.store;

				(0, _invariant2.default)(_this.store, "Could not find \"store\" in either the context or " + ("props of \"" + secureDisplayName + "\". ") + "Either wrap the root component in a <Provider>, " + ("or explicitly pass \"store\" as a prop to \"" + secureDisplayName + "\"."));

				var storeState = _this.store.getState();
				_this.state = { storeState: storeState };
				_this.clearCache();
				return _this;
			}

			_createClass(Secure, [{
				key: "computeStateProps",
				value: function computeStateProps(store, props) {
					return this.configureFinalMapState(store, props);
				}
			}, {
				key: "configureFinalMapState",
				value: function configureFinalMapState(store, props) {
					var state = store.getState();
					var shouldBeDisplayed = mapState(state, secure.prototype.rules, runCreator(state));

					this.finalMapStateToProps = function () {
						return { shouldBeDisplayed: shouldBeDisplayed };
					};
					this.doStatePropsDependOnOwnProps = this.finalMapStateToProps.length !== 1;

					return { shouldBeDisplayed: shouldBeDisplayed };
				}
			}, {
				key: "computeDispatchProps",
				value: function computeDispatchProps(store, props) {
					if (!this.finalMapDispatchToProps) {
						return this.configureFinalMapDispatch(store, props);
					}

					var dispatch = store.dispatch;

					var dispatchProps = this.doDispatchPropsDependOnOwnProps ? this.finalMapDispatchToProps(dispatch, props) : this.finalMapDispatchToProps(dispatch);

					return dispatchProps;
				}
			}, {
				key: "configureFinalMapDispatch",
				value: function configureFinalMapDispatch(store, props) {
					var mappedDispatch = mapDispatch(store.dispatch, props);
					var isFactory = typeof mappedDispatch === 'function';

					this.finalMapDispatchToProps = isFactory ? mappedDispatch : mapDispatch;
					this.doDispatchPropsDependOnOwnProps = this.finalMapDispatchToProps.length !== 1;

					if (isFactory) {
						return this.computeDispatchProps(store, props);
					}

					return mappedDispatch;
				}
			}, {
				key: "updateStatePropsIfNeeded",
				value: function updateStatePropsIfNeeded() {
					var nextStateProps = this.computeStateProps(this.store, this.props);
					if (this.stateProps && (0, _shallowEqual2.default)(nextStateProps, this.stateProps)) {
						return false;
					}

					this.stateProps = nextStateProps;
					return true;
				}
			}, {
				key: "updateDispatchPropsIfNeeded",
				value: function updateDispatchPropsIfNeeded() {
					var nextDispatchProps = this.computeDispatchProps(this.store, this.props);
					if (this.dispatchProps && (0, _shallowEqual2.default)(nextDispatchProps, this.dispatchProps)) {
						return false;
					}

					this.dispatchProps = nextDispatchProps;
					return true;
				}
			}, {
				key: "updateMergedPropsIfNeeded",
				value: function updateMergedPropsIfNeeded() {
					var nextMergedProps = computeMergedProps(this.stateProps, this.dispatchProps, this.props);
					if (this.mergedProps && checkMergedEquals && (0, _shallowEqual2.default)(nextMergedProps, this.mergedProps)) {
						return false;
					}

					this.mergedProps = nextMergedProps;
					return true;
				}
			}, {
				key: "isSubscribed",
				value: function isSubscribed() {
					return typeof this.unsubscribe === 'function';
				}
			}, {
				key: "trySubscribe",
				value: function trySubscribe() {
					if (shouldSubscribe && !this.unsubscribe) {
						this.unsubscribe = this.store.subscribe(this.handleChange.bind(this));
						this.handleChange();
					}
				}
			}, {
				key: "tryUnsubscribe",
				value: function tryUnsubscribe() {
					if (this.unsubscribe) {
						this.unsubscribe();
						this.unsubscribe = null;
					}
				}
			}, {
				key: "componentDidMount",
				value: function componentDidMount() {
					this.trySubscribe();
				}
			}, {
				key: "componentWillReceiveProps",
				value: function componentWillReceiveProps(nextProps) {
					if (!pure || !(0, _shallowEqual2.default)(nextProps, this.props)) {
						this.haveOwnPropsChanged = true;
					}
				}
			}, {
				key: "componentWillUnmount",
				value: function componentWillUnmount() {
					this.tryUnsubscribe();
					this.clearCache();
				}
			}, {
				key: "clearCache",
				value: function clearCache() {
					this.dispatchProps = null;
					this.stateProps = null;
					this.mergedProps = null;
					this.haveOwnPropsChanged = true;
					this.hasStoreStateChanged = true;
					this.haveStatePropsBeenPrecalculated = false;
					this.statePropsPrecalculationError = null;
					this.renderedElement = null;
					this.finalMapDispatchToProps = null;
					this.finalMapStateToProps = null;
				}
			}, {
				key: "handleChange",
				value: function handleChange() {
					if (!this.unsubscribe) {
						return;
					}

					var storeState = this.store.getState();
					var prevStoreState = this.state.storeState;
					if (pure && prevStoreState === storeState) {
						return;
					}

					if (pure && !this.doStatePropsDependOnOwnProps) {
						var haveStatePropsChanged = tryCatch(this.updateStatePropsIfNeeded, this);
						if (!haveStatePropsChanged) {
							return;
						}
						if (haveStatePropsChanged === errorObject) {
							this.statePropsPrecalculationError = errorObject.value;
						}
						this.haveStatePropsBeenPrecalculated = true;
					}

					this.hasStoreStateChanged = true;
					this.setState({ storeState: storeState });
				}
			}, {
				key: "getWrappedInstance",
				value: function getWrappedInstance() {
					(0, _invariant2.default)(withRef, "To access the wrapped instance, you need to specify " + "{ withRef: true } as the fourth argument of the secure() call.");

					return this.refs.wrappedInstance;
				}
			}, {
				key: "render",
				value: function render() {
					var haveOwnPropsChanged = this.haveOwnPropsChanged,
					    hasStoreStateChanged = this.hasStoreStateChanged,
					    haveStatePropsBeenPrecalculated = this.haveStatePropsBeenPrecalculated,
					    statePropsPrecalculationError = this.statePropsPrecalculationError,
					    renderedElement = this.renderedElement;


					this.haveOwnPropsChanged = false;
					this.hasStoreStateChanged = false;
					this.haveStatePropsBeenPrecalculated = false;
					this.statePropsPrecalculationError = null;

					if (statePropsPrecalculationError) {
						throw statePropsPrecalculationError;
					}

					var shouldUpdateStateProps = true;
					var shouldUpdateDispatchProps = true;
					if (pure && renderedElement) {
						shouldUpdateStateProps = hasStoreStateChanged || haveOwnPropsChanged && this.doStatePropsDependOnOwnProps;
						shouldUpdateDispatchProps = haveOwnPropsChanged && this.doDispatchPropsDependOnOwnProps;
					}

					var haveStatePropsChanged = false;
					var haveDispatchPropsChanged = false;
					if (haveStatePropsBeenPrecalculated) {
						haveStatePropsChanged = true;
					} else if (shouldUpdateStateProps) {
						haveStatePropsChanged = this.updateStatePropsIfNeeded();
					}
					if (shouldUpdateDispatchProps) {
						haveDispatchPropsChanged = this.updateDispatchPropsIfNeeded();
					}

					var haveMergedPropsChanged = true;
					if (haveStatePropsChanged || haveDispatchPropsChanged || haveOwnPropsChanged) {
						haveMergedPropsChanged = this.updateMergedPropsIfNeeded();
					} else {
						haveMergedPropsChanged = false;
					}

					var shouldBeDisplayed = this.mergedProps.shouldBeDisplayed;


					if (!haveMergedPropsChanged && renderedElement) {
						return renderedElement;
					}

					if (withRef) {
						this.renderedElement = (0, _react.createElement)(WrappedComponent, _extends({}, this.mergedProps, {
							ref: 'wrappedInstance'
						}));
					} else {
						this.renderedElement = (0, _react.createElement)(WrappedComponent, this.mergedProps);

						if (!shouldBeDisplayed) {
							this.renderedElement = _react2.default.createElement("div", { style: { display: "none" } });
						}
					}

					return this.renderedElement;
				}
			}]);

			return Secure;
		}(_react.Component);

		Secure.displayName = secureDisplayName;
		Secure.WrappedComponent = WrappedComponent;
		Secure.contextTypes = {
			store: _storeShape2.default
		};
		Secure.propTypes = {
			store: _storeShape2.default
		};

		if (process.env.NODE_ENV !== 'production') {
			Secure.prototype.componentWillUpdate = function componentWillUpdate() {
				if (this.version === version) {
					return;
				}

				// We are hot reloading!
				this.version = version;
				this.trySubscribe();
				this.clearCache();
			};
		}

		return (0, _hoistNonReactStatics2.default)(Secure, WrappedComponent);
	};
}
secure.prototype.rules = {};