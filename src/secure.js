import React, {Component, createElement} from "react";
import storeShape from "react-redux/lib/utils/storeShape";
import shallowEqual from "react-redux/lib/utils/shallowEqual";
import hoistStatics from "hoist-non-react-statics";
import invariant from "invariant";

const defaultMapStateToProps = state => ({}) // eslint-disable-line no-unused-vars
const defaultMapDispatchToProps = dispatch => ({dispatch})
const defaultMergeProps = (stateProps, dispatchProps, parentProps) => ({
	...parentProps,
	...stateProps,
	...dispatchProps
});

function getDisplayName(WrappedComponent) {
	return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

let errorObject = {value: null};
function tryCatch(fn, ctx) {
	try {
		return fn.apply(ctx);
	} catch (e) {
		errorObject.value = e;
		return errorObject;
	}
}

function runCreator(state) {
	let counter = 0;
	let failed = [];
	return function run(...rules) {
		if (rules.length == 0)
			throw new Error("provide some rules for run function");

		rules.forEach(rule => {
			rule(state, (ruleName) => counter += 1, (ruleName) => failed.push(ruleName))
		});

		return !failed.length > 0;
	}
}


// Helps track hot reloading.
let nextVersion = 0;
export default function secure(mapStateToProps, mapDispatchToProps, mergeProps, options = {}) {
	const shouldSubscribe = Boolean(mapStateToProps);
	const mapState = mapStateToProps || defaultMapStateToProps;

	let mapDispatch;
	if (typeof mapDispatchToProps === 'function') {
		mapDispatch = mapDispatchToProps;
	} else if (!mapDispatchToProps) {
		mapDispatch = defaultMapDispatchToProps;
	} else {
		mapDispatch = wrapActionCreators(mapDispatchToProps);
	}

	const finalMergeProps = mergeProps || defaultMergeProps;
	const {pure = true, withRef = false} = options;
	const checkMergedEquals = pure && finalMergeProps !== defaultMergeProps;

	// Helps track hot reloading.
	const version = nextVersion++;

	return function wrapWithSecure(WrappedComponent) {
		const secureDisplayName = `Secure(${getDisplayName(WrappedComponent)})`;

		function computeMergedProps(stateProps, dispatchProps, parentProps) {
			const mergedProps = finalMergeProps(stateProps, dispatchProps, parentProps);

			return mergedProps;
		}

		class Secure extends Component {
			shouldComponentUpdate() {
				return !pure || this.haveOwnPropsChanged || this.hasStoreStateChanged
			}

			constructor(props, context) {
				super(props, context);
				this.version = version;
				this.store = props.store || context.store;

				invariant(this.store,
					`Could not find "store" in either the context or ` +
					`props of "${secureDisplayName}". ` +
					`Either wrap the root component in a <Provider>, ` +
					`or explicitly pass "store" as a prop to "${secureDisplayName}".`
				);

				const storeState = this.store.getState();
				this.state = {storeState};
				this.clearCache();
			}

			computeStateProps(store, props) {
				return this.configureFinalMapState(store, props);
			}

			configureFinalMapState(store, props) {
				const state = store.getState();
				const shouldBeDisplayed = mapState(state, secure.prototype.rules, runCreator(state));

				this.finalMapStateToProps = () => {
					return {shouldBeDisplayed};
				};
				this.doStatePropsDependOnOwnProps = this.finalMapStateToProps.length !== 1;

				return {shouldBeDisplayed};
			}

			computeDispatchProps(store, props) {
				if (!this.finalMapDispatchToProps) {
					return this.configureFinalMapDispatch(store, props);
				}

				const {dispatch} = store;
				const dispatchProps = this.doDispatchPropsDependOnOwnProps ?
					this.finalMapDispatchToProps(dispatch, props) :
					this.finalMapDispatchToProps(dispatch);

				return dispatchProps;
			}

			configureFinalMapDispatch(store, props) {
				const mappedDispatch = mapDispatch(store.dispatch, props);
				const isFactory = typeof mappedDispatch === 'function';

				this.finalMapDispatchToProps = isFactory ? mappedDispatch : mapDispatch;
				this.doDispatchPropsDependOnOwnProps = this.finalMapDispatchToProps.length !== 1;

				if (isFactory) {
					return this.computeDispatchProps(store, props);
				}

				return mappedDispatch;
			}

			updateStatePropsIfNeeded() {
				const nextStateProps = this.computeStateProps(this.store, this.props);
				if (this.stateProps && shallowEqual(nextStateProps, this.stateProps)) {
					return false;
				}

				this.stateProps = nextStateProps;
				return true;
			}

			updateDispatchPropsIfNeeded() {
				const nextDispatchProps = this.computeDispatchProps(this.store, this.props);
				if (this.dispatchProps && shallowEqual(nextDispatchProps, this.dispatchProps)) {
					return false;
				}

				this.dispatchProps = nextDispatchProps;
				return true;
			}

			updateMergedPropsIfNeeded() {
				const nextMergedProps = computeMergedProps(this.stateProps, this.dispatchProps, this.props);
				if (this.mergedProps && checkMergedEquals && shallowEqual(nextMergedProps, this.mergedProps)) {
					return false;
				}

				this.mergedProps = nextMergedProps;
				return true;
			}

			isSubscribed() {
				return typeof this.unsubscribe === 'function';
			}

			trySubscribe() {
				if (shouldSubscribe && !this.unsubscribe) {
					this.unsubscribe = this.store.subscribe(this.handleChange.bind(this));
					this.handleChange();
				}
			}

			tryUnsubscribe() {
				if (this.unsubscribe) {
					this.unsubscribe();
					this.unsubscribe = null;
				}
			}

			componentDidMount() {
				this.trySubscribe();
			}

			componentWillReceiveProps(nextProps) {
				if (!pure || !shallowEqual(nextProps, this.props)) {
					this.haveOwnPropsChanged = true;
				}
			}

			componentWillUnmount() {
				this.tryUnsubscribe();
				this.clearCache();
			}

			clearCache() {
				this.dispatchProps = null;
				this.stateProps = null;
				this.mergedProps = null;
				this.haveOwnPropsChanged = true;
				this.hasStoreStateChanged = true
				this.haveStatePropsBeenPrecalculated = false;
				this.statePropsPrecalculationError = null;
				this.renderedElement = null;
				this.finalMapDispatchToProps = null;
				this.finalMapStateToProps = null;
			}

			handleChange() {
				if (!this.unsubscribe) {
					return;
				}

				const storeState = this.store.getState();
				const prevStoreState = this.state.storeState;
				if (pure && prevStoreState === storeState) {
					return;
				}

				if (pure && !this.doStatePropsDependOnOwnProps) {
					const haveStatePropsChanged = tryCatch(this.updateStatePropsIfNeeded, this);
					if (!haveStatePropsChanged) {
						return;
					}
					if (haveStatePropsChanged === errorObject) {
						this.statePropsPrecalculationError = errorObject.value;
					}
					this.haveStatePropsBeenPrecalculated = true;
				}

				this.hasStoreStateChanged = true;
				this.setState({storeState});
			}

			getWrappedInstance() {
				invariant(withRef,
					`To access the wrapped instance, you need to specify ` +
					`{ withRef: true } as the fourth argument of the secure() call.`
				);

				return this.refs.wrappedInstance;
			}

			render() {
				const {
					haveOwnPropsChanged,
					hasStoreStateChanged,
					haveStatePropsBeenPrecalculated,
					statePropsPrecalculationError,
					renderedElement
				} = this;

				this.haveOwnPropsChanged = false;
				this.hasStoreStateChanged = false;
				this.haveStatePropsBeenPrecalculated = false;
				this.statePropsPrecalculationError = null;

				if (statePropsPrecalculationError) {
					throw statePropsPrecalculationError;
				}

				let shouldUpdateStateProps = true;
				let shouldUpdateDispatchProps = true;
				if (pure && renderedElement) {
					shouldUpdateStateProps = hasStoreStateChanged || (
							haveOwnPropsChanged && this.doStatePropsDependOnOwnProps
						);
					shouldUpdateDispatchProps =
						haveOwnPropsChanged && this.doDispatchPropsDependOnOwnProps;
				}

				let haveStatePropsChanged = false;
				let haveDispatchPropsChanged = false;
				if (haveStatePropsBeenPrecalculated) {
					haveStatePropsChanged = true;
				} else if (shouldUpdateStateProps) {
					haveStatePropsChanged = this.updateStatePropsIfNeeded();
				}
				if (shouldUpdateDispatchProps) {
					haveDispatchPropsChanged = this.updateDispatchPropsIfNeeded();
				}

				let haveMergedPropsChanged = true;
				if (
					haveStatePropsChanged ||
					haveDispatchPropsChanged ||
					haveOwnPropsChanged
				) {
					haveMergedPropsChanged = this.updateMergedPropsIfNeeded();
				} else {
					haveMergedPropsChanged = false;
				}

				let {shouldBeDisplayed} = this.mergedProps;

				if (!haveMergedPropsChanged && renderedElement) {
					return renderedElement;
				}

				if (withRef) {
					this.renderedElement = createElement(WrappedComponent, {
						...this.mergedProps,
						ref: 'wrappedInstance'
					});
				} else {
					this.renderedElement = createElement(WrappedComponent,
						this.mergedProps
					);

					if(!shouldBeDisplayed) {
						this.renderedElement = <div style={{display:"none"}}></div>
					}
				}


				return this.renderedElement;
			}
		}

		Secure.displayName = secureDisplayName;
		Secure.WrappedComponent = WrappedComponent;
		Secure.contextTypes = {
			store: storeShape
		};
		Secure.propTypes = {
			store: storeShape
		};

		if (process.env.NODE_ENV !== 'production') {
			Secure.prototype.componentWillUpdate = function componentWillUpdate() {
				if (this.version === version) {
					return;
				}

				// We are hot reloading!
				this.version = version;
				this.trySubscribe();
				this.clearCache();
			}
		}

		return hoistStatics(Secure, WrappedComponent);
	}
}
secure.prototype.rules = {};
