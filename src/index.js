import React, {Component} from "react";
import Secure from "./secure";

function secureReducer(rules) {
	Secure.prototype.rules = rules;
	return (state = false, action) => {
		switch (action.type) {
			case "USER_IMPERSONATE":
				return state;
		}

		return state;
	};
}


module.exports = {
	secure: Secure,
	secureReducer
};

