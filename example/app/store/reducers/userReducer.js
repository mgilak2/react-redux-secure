export default (state = {rules: {comments: true, editComments: true}}, action) => {
	switch (action.type) {
		case "UPDATE_USER_RULE":
			return {rules: {comments: true, editComments: !state.rules.editComments}};
	}
	return state;
};
