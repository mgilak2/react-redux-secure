import {combineReducers, createStore, applyMiddleware} from "redux";
import {counterReducer} from "./reducers/counterReducer";
import userReducer from "./reducers/userReducer";
import createLogger from "redux-logger";
import {secureReducer} from "../../../dist";

export const store = createStore(
	combineReducers({
		counter: counterReducer,
		user: userReducer,
		secure: secureReducer(require("../rules").default)
	}),
	applyMiddleware(createLogger())
);
