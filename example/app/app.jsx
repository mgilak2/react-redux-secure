import {store} from "./store/store";
import {Provider} from "react-redux";
import React from 'react';
import Dashboard from "./containers/Dashboard";

export default class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<Dashboard/>
			</Provider>
		)
	}
}
