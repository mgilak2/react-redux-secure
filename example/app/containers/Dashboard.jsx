import React, {Component} from "react";
import {connect} from 'react-redux';
import Comments from './Comments';

@connect(({counter}) => ({counter}))
export default class Dashboard extends Component {
	render() {
		let {dispatch} = this.props;
		return (
			<div>
				<button onClick={() => dispatch({type: "UPDATE_USER_RULE"})}>toggle edit comment</button>

				<Comments/>
			</div>
		);
	}
}
