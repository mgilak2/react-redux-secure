import React, {Component} from "react";
import {connect} from "react-redux";
import {secure} from "../../../dist";

@connect(({counter}) => ({counter}))
@secure(({user}, {comments, editComments}, run) => run(editComments(), comments()))
export default class Comments extends Component {
	render() {
		return (
			<div>comments</div>
		);
	}
}
